import tensorflow as tf
from tensorflow.keras.layers.experimental.preprocessing import Resizing
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.inception_v3 import preprocess_input
from scipy.linalg import sqrtm
from skimage.transform import resize
import numpy as np
import os
import PIL
from PIL import Image
import glob
import imageio


# DATASET

def _extract_fn(tfrecord):
    # Extract features
    features = {
        'filename': tf.io.FixedLenFeature([], tf.string),
        'image': tf.io.FixedLenFeature([64 * 64 * 3], tf.int64),
        'attrs': tf.io.FixedLenFeature([40], tf.float32)
    }
    # Extract the data record
    sample = tf.io.parse_single_example(tfrecord, features)
    # return sample
    filename = sample['filename']
    image = sample['image']
    attrs = sample['attrs']
    image = tf.reshape(image, [64, 64, 3])
    image = tf.cast(image, tf.float32)
    image = (image / 127.5) - 1
    return image


def read_tf_dataset(path, batch_size):
    images_names = os.listdir(path)
    images_paths = [os.path.join(path, image_path) for image_path in images_names]

    dataset = tf.data.TFRecordDataset(images_paths)
    dataset = dataset.map(_extract_fn)
    dataset = dataset.repeat()
    dataset = dataset.batch(batch_size)
    return dataset

def _extract_fn_with_attrs(tfrecord):
    # Extract features
    features = {
        'filename': tf.io.FixedLenFeature([], tf.string),
        'image': tf.io.FixedLenFeature([64 * 64 * 3], tf.int64),
        'attrs': tf.io.FixedLenFeature([40], tf.float32)
    }
    # Extract the data record
    sample = tf.io.parse_single_example(tfrecord, features)
    # return sample
    filename = sample['filename']
    image = sample['image']
    attrs = tf.gather(sample['attrs'], [16, 21])
    image = tf.reshape(image, [64, 64, 3])
    image = tf.cast(image, tf.float32)
    image = (image / 127.5) - 1
    return image, attrs


def read_tf_dataset_with_attrs(path, batch_size):
    images_names = os.listdir(path)
    images_paths = [os.path.join(path, image_path) for image_path in images_names]

    dataset = tf.data.TFRecordDataset(images_paths)
    dataset = dataset.map(_extract_fn_with_attrs)
    dataset = dataset.repeat()
    dataset = dataset.batch(batch_size)
    return dataset


## Image processing functions

# for equal square images
def save_grid_of_images(img_batch, rows, cols, path, image_size=64):
    new_image = Image.new('RGB', (cols * image_size, rows * image_size))
    img_count = img_batch.shape[0]
    for row in range(rows):
        for col in range(cols):
            if row * cols + col >= img_count:
                break
            cur_image = Image.fromarray(
                img_batch[row * cols + col, :, :, :].numpy().reshape((64, 64, 3)).astype(np.uint8))
            new_image.paste(cur_image, (col * image_size, row * image_size))
    new_image.save(path, "PNG")


def generate_and_save_images(model, epoch, test_input, folder):
    predictions = model(test_input, training=False)
    predictions = (predictions + 1) * 127.5
    rows = int(predictions.shape[0] / 8) + 1
    cols = 8
    path = '{}/image_at_epoch_{:04d}.png'.format(folder, epoch)
    save_grid_of_images(predictions, rows, cols, path)


def make_gif(anim_file, images_wildcard):
    with imageio.get_writer(anim_file, mode='I') as writer:
        filenames = glob.glob(images_wildcard)
        filenames = sorted(filenames)
        for filename in filenames:
            image = imageio.imread(filename)
            writer.append_data(image)
        image = imageio.imread(filename)
        writer.append_data(image)


### FID

# For InceptionV3 model
def scale_images(images, new_shape):
    images_list = list()
    for image in images:
        # resize with nearest neighbor interpolation
        new_image = resize(image, new_shape, 0)
        # store
        images_list.append(new_image)
    return np.asarray(images_list)


def calculate_fid(model, images1, images2, mu1=None, sigma1=None):
    if mu1 is None:
        act1 = model.predict(images1)
        # calculate mean and covariance statistics
        mu1, sigma1 = act1.mean(axis=0), np.cov(act1, rowvar=False)
    act2 = model.predict(images2)
    mu2, sigma2 = act2.mean(axis=0), np.cov(act2, rowvar=False)
    # calculate sum squared difference between means
    ssdiff = np.sum((mu1 - mu2) ** 2.0)
    # calculate sqrt of product between cov
    covmean = sqrtm(sigma1.dot(sigma2))
    # check and correct imaginary numbers from sqrt
    if np.iscomplexobj(covmean):
        covmean = covmean.real
    # calculate score
    fid = ssdiff + np.trace(sigma1 + sigma2 - 2.0 * covmean)
    return fid


def fid_wrapper(dataset, generator, fid_batch, batch_size, noise_dim):
    mu1 = None
    sigma1 = None
    model = InceptionV3(include_top=False, pooling='avg', input_shape=(299, 299, 3))

    def fid_function():
        nonlocal mu1
        nonlocal sigma1
        if mu1 is None:
            images1_list = []
            for i, image_batch in enumerate(dataset):
                if i == int(fid_batch / batch_size):
                    break
                images1_list.append(image_batch)
            images1 = np.concatenate(images1_list, axis=0)
            images1 = (images1 + 1) * 127.5
            images1 = scale_images(images1, (299, 299, 3))
            images1 = preprocess_input(images1)
            act1 = model.predict(images1)
            mu1, sigma1 = act1.mean(axis=0), np.cov(act1, rowvar=False)
        images2_list = []
        for i in range(int(fid_batch / batch_size)):
            noise = tf.random.normal([batch_size, noise_dim])
            images2_list.append(generator(noise, training=False))
        images2 = np.concatenate(images2_list, axis=0)
        images2 = (images2 + 1) * 127.5
        images2 = scale_images(images2, (299, 299, 3))
        images2 = preprocess_input(images2)
        fid = calculate_fid(model, None, images2, mu1=mu1, sigma1=sigma1)
        return fid

    return fid_function

def fid_wrapper_with_attrs(dataset, generator, fid_batch, batch_size, noise_dim):
    mu1 = None
    sigma1 = None
    model = InceptionV3(include_top=False, pooling='avg', input_shape=(299, 299, 3))

    def fid_function():
        nonlocal mu1
        nonlocal sigma1
        if mu1 is None:
            images1_list = []
            for i, batch in enumerate(dataset):
                image_batch, attrs_batch = batch
                if i == int(fid_batch / batch_size):
                    break
                images1_list.append(image_batch)
            images1 = np.concatenate(images1_list, axis=0)
            images1 = (images1 + 1) * 127.5
            images1 = scale_images(images1, (299, 299, 3))
            images1 = preprocess_input(images1)
            act1 = model.predict(images1)
            mu1, sigma1 = act1.mean(axis=0), np.cov(act1, rowvar=False)
        images2_list = []
        for i in range(int(fid_batch / batch_size)):
            noise = tf.random.normal([batch_size, noise_dim])
            cond = np.array([[[-1, -1]] * 4, [[-1, +1]] * 4, [[+1, -1]] * 4, [[+1, +1]] * 4]).astype(np.float32)
            cond = np.reshape(cond, (16, 2))
            noise = np.concatenate((noise, cond), axis=1)
            images2_list.append(generator(noise, training=False))
        images2 = np.concatenate(images2_list, axis=0)
        images2 = (images2 + 1) * 127.5
        images2 = scale_images(images2, (299, 299, 3))
        images2 = preprocess_input(images2)
        fid = calculate_fid(model, None, images2, mu1=mu1, sigma1=sigma1)
        return fid

    return fid_function
