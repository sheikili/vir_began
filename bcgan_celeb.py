import tensorflow as tf
import glob
import imageio
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
from PIL import Image
from tensorflow.keras import layers
import time
import pandas as pd
from scipy.linalg import sqrtm
from skimage.transform import resize
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.layers.experimental.preprocessing import Resizing

# from IPython import display
from utils import read_tf_dataset, fid_wrapper, generate_and_save_images, make_gif

BUFFER_SIZE = 60000
BATCH_SIZE = 16
FID_BATCH = 1000

EPOCHS = 100
noise_dim = 100
num_examples_to_generate = 16
num_batches = 1000

tf_records_dir = 'tf_records_64'
checkpoint_dir = './bcgan_training_checkpoints'
anim_file = 'bcgan.gif'
images_folder = "bcgan_images"
images_wildcard = 'bcgan_images/image*.png'
fid_output = 'fid_bcgan.txt'

generator_optimizer = tf.keras.optimizers.Adam(1e-4, beta_1=0.5)
discriminator_optimizer = tf.keras.optimizers.Adam(1e-4)
# This method returns a helper function to compute cross entropy loss
cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)

# We will reuse this seed overtime (so it's easier)
# to visualize progress in the animated GIF)
seed = tf.random.normal([num_examples_to_generate, noise_dim])
train_dataset = read_tf_dataset(tf_records_dir, BATCH_SIZE)


def make_bcgan_generator_model():
    filter_number = 64
    w = 64
    im_height = 8
    im_width = 8

    model = tf.keras.Sequential()
    model.add(layers.Dense(im_height * im_width * filter_number, use_bias=True, input_shape=(100,)))
    model.add(layers.Reshape((im_height, im_width, filter_number)))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w // 4, w // 4))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w // 2, w // 2))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w, w))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(3, (3, 3), strides=(1, 1), padding='same', use_bias=True))

    return model


def make_bcgan_discriminator_model():
    filter_number = 64
    h = 1

    # h0
    model = tf.keras.Sequential()
    model.add(layers.InputLayer(input_shape=(64, 64, 3)))
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(2 * filter_number, (1, 1), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(2 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(2 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(3 * filter_number, (1, 1), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))

    model.add(layers.Conv2D(3 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(3 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(4 * filter_number, (1, 1), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(4 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(4 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Flatten())
    model.add(layers.Dense(1))

    return model


generator = make_bcgan_generator_model()
discriminator = make_bcgan_discriminator_model()


def discriminator_loss(real_output, fake_output):
    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss


def generator_loss(fake_output):
    return cross_entropy(tf.ones_like(fake_output), fake_output)


# Notice the use of `tf.function`
# This annotation causes the function to be "compiled".
@tf.function
def train_step(images):
    noise = tf.random.normal([BATCH_SIZE, noise_dim])

    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        generated_images = generator(noise, training=True)

        real_output = discriminator(images, training=True)
        fake_output = discriminator(generated_images, training=True)
        gen_loss = generator_loss(fake_output)
        disc_loss = discriminator_loss(real_output, fake_output)

    gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
    gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)

    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))


checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                 discriminator_optimizer=discriminator_optimizer,
                                 generator=generator,
                                 discriminator=discriminator)


def train(dataset, epochs):
    fid_file = open(fid_output, 'w')
    fid_calc = fid_wrapper(dataset, generator, FID_BATCH, BATCH_SIZE, noise_dim)

    for epoch in range(epochs):
        start = time.time()

        for i, image_batch in enumerate(dataset):
            if i % 100 == 0:
                print(f'Epoch:{epoch}, batch: {i}')
            if i == num_batches:
                break
            train_step(image_batch)

        # FID
        if (epoch + 1) % 5 == 0:
            fid_start = time.time()
            fid = fid_calc()
            print(f'Epoch:{epoch}; FID: {fid}; time: {time.time() - fid_start} \n')
            fid_file.write(f"Epoch:{epoch}; FID: {fid}; time: {time.time() - fid_start} \n")

        ## Produce images for the GIF as we go
        # display.clear_output(wait=True)
        generate_and_save_images(generator, epoch + 1, seed, images_folder)

        # Save the model every 10 epochs
        if (epoch + 1) % 10 == 0:
            checkpoint.save(file_prefix=checkpoint_prefix)
            print('Time for epoch {} is {} sec'.format(epoch + 1, time.time() - start))

    ## Generate after the final epoch
    #   display.clear_output(wait=True)
    generate_and_save_images(generator, epochs, seed, images_folder)


if __name__ == '__main__':
    print("Start training ...")
    train(train_dataset, EPOCHS)
    checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))
    make_gif(anim_file, images_wildcard)
