import tensorflow as tf
import glob
import imageio
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
from PIL import Image
from tensorflow.keras import layers
import time
import pandas as pd
from scipy.linalg import sqrtm
from skimage.transform import resize
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.layers.experimental.preprocessing import Resizing

# from IPython import display
from utils import read_tf_dataset, fid_wrapper, generate_and_save_images, make_gif

BUFFER_SIZE = 60000
BATCH_SIZE = 16
FID_BATCH = 1000

EPOCHS = 100
noise_dim = 100
num_examples_to_generate = 16
num_batches = 1000

# BEGAN
GAMMA = 0.5
LAMBDA = 0.001
NSNAPSHOT = 2440

LR = 1e-4
MOMENTUM = 0.5

tf_records_dir = 'tf_records_64'
checkpoint_dir = './began_training_checkpoints'
anim_file = 'began.gif'
images_folder = "began_images"
images_wildcard = 'began_images/image*.png'
fid_output = 'fid_began.txt'

generator_optimizer = tf.keras.optimizers.Adam(LR, beta_1=MOMENTUM)
discriminator_optimizer = tf.keras.optimizers.Adam(LR, beta_1=MOMENTUM)

# This method returns a helper function to compute cross entropy loss
cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)

# We will reuse this seed overtime (so it's easier)
# to visualize progress in the animated GIF)
seed = tf.random.normal([num_examples_to_generate, noise_dim])
train_dataset = read_tf_dataset(tf_records_dir, BATCH_SIZE)


def make_began_generator_model():
    filter_number = 64
    w = 64
    im_height = 8
    im_width = 8

    model = tf.keras.Sequential()
    model.add(layers.Dense(im_height * im_width * filter_number, use_bias=True, ))
    model.add(layers.Reshape((im_height, im_width, filter_number)))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w // 4, w // 4))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w // 2, w // 2))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w, w))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(3, (3, 3), strides=(1, 1), padding='same', use_bias=True))

    return model


def make_began_discriminator_encoder_model():
    filter_number = 64
    h = 1

    model = tf.keras.Sequential()
    model.add(layers.InputLayer(input_shape=(64, 64, 3)))
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(2 * filter_number, (1, 1), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(2 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(2 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(3 * filter_number, (1, 1), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))

    model.add(layers.Conv2D(3 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(3 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(4 * filter_number, (1, 1), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(4 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(4 * filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Flatten())
    model.add(layers.Dense(64))

    return model


def make_began_discriminator_decoder_model():
    filter_number = 64
    w = 64
    im_height = 8
    im_width = 8

    model = tf.keras.Sequential()
    model.add(layers.Dense(im_height * im_width * filter_number, use_bias=True, ))
    model.add(layers.Reshape((im_height, im_width, filter_number)))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w // 4, w // 4))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w // 2, w // 2))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(Resizing(w, w))

    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())
    model.add(layers.Conv2D(filter_number, (3, 3), strides=(1, 1), padding='same', use_bias=True))
    model.add(layers.ELU())

    model.add(layers.Conv2D(3, (3, 3), strides=(1, 1), padding='same', use_bias=True))

    return model


def make_began_discriminator_model():
    encoder = make_began_discriminator_encoder_model()
    decoder = make_began_discriminator_decoder_model()
    # ---
    model = tf.keras.Sequential()
    model.add(encoder)
    model.add(decoder)
    return model


generator = make_began_generator_model()
discriminator = make_began_discriminator_model()

@tf.function
def discriminator_loss(real_output, fake_output):
    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss

@tf.function
def generator_loss(fake_output):
    return cross_entropy(tf.ones_like(fake_output), fake_output)

@tf.function
def l1_loss(x, y):
    return tf.reduce_mean(tf.abs(x - y))


# Notice the use of `tf.function`
# This annotation causes the function to be "compiled".
@tf.function
def train_step(images, kt):
    noise = tf.random.normal([BATCH_SIZE, noise_dim])

    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        ## noise - input to generator
        ## images - true images
        generated_images = generator(noise, training=True)

        real_output = discriminator(images, training=True)
        fake_output = discriminator(generated_images, training=True)

        # gen_loss = generator_loss(fake_output)
        # disc_loss = discriminator_loss(real_output, fake_output)

        recon_generated = generator(noise, training=True)

        # Discriminator (Critic)
        d_real = discriminator(images)
        d_fake = discriminator(recon_generated)
        # recon_dec = decoder(x)

        # Loss
        d_real_loss = l1_loss(images, d_real)
        d_fake_loss = l1_loss(recon_generated, d_fake)
        d_loss = d_real_loss - tf.cast(kt, 'float32') * d_fake_loss
        g_loss = d_fake_loss

        gen_loss = g_loss
        disc_loss = d_loss

        # # update kt, m_global
        # kt = np.maximum(np.minimum(1., kt + _lambda * (gamma * d_real_loss - d_fake_loss)), 0.)
        # m_global = d_real_loss + np.abs(gamma * d_real_loss - d_fake_loss)
        # loss = loss_g + loss_d

        # print("Epoch: [%2d] [%4d/%4d] time: %4.4f, "
        #               "loss: %.4f, loss_g: %.4f, loss_d: %.4f, d_real: %.4f, d_fake: %.4f, kt: %.8f, M: %.8f"
        #               % (epoch, idx, batch_idxs, time.time() - start_time,
        #                  loss, loss_g, loss_d, d_real_loss, d_fake_loss, kt, m_global))

    gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
    gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)

    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))

    return d_real_loss, d_fake_loss


checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                 discriminator_optimizer=discriminator_optimizer,
                                 generator=generator,
                                 discriminator=discriminator)


def train(dataset, epochs):
    global GAMMA
    global LAMBDA
    global NSNAPSHOT

    fid_file = open(fid_output, 'w')
    fid_calc = fid_wrapper(dataset, generator, FID_BATCH, BATCH_SIZE, noise_dim)

    kt = np.float64(0.)

    count = 0

    for epoch in range(epochs):
        start = time.time()

        for i, image_batch in enumerate(dataset):
            count += 1
            if i % 100 == 0:
                print(f'Epoch:{epoch}, batch: {i}')

            if i == num_batches:
                break

            d_real_loss, d_fake_loss = train_step(image_batch, tf.convert_to_tensor(kt, dtype=tf.float64))

            kt = np.maximum(np.minimum(1., kt + LAMBDA * (GAMMA * d_real_loss - d_fake_loss)), 0.)
            m_global = d_real_loss + np.abs(GAMMA * d_real_loss - d_fake_loss)
            loss = d_fake_loss + d_real_loss

        # Produce images for the GIF as we go
        #     display.clear_output(wait=True)
        generate_and_save_images(generator, epoch + 1, seed, images_folder)

        # Save the model every 10 epochs
        if (epoch + 1) % 10 == 0:
            checkpoint.save(file_prefix=checkpoint_prefix)
            print('Time for epoch {} is {} sec'.format(epoch + 1, time.time() - start))

        if (epoch + 1) % 5 == 0:
            fid_start = time.time()
            fid = fid_calc()
            print(f'\nEpoch:{epoch}; FID: {fid}; time: {time.time() - fid_start} ', end='')
            fid_file.write(f"Epoch:{epoch}; FID: {fid}; time: {time.time() - fid_start} \n")

            print(f'kt = {kt}, m_global = {m_global}, loss = {loss}\n')

        # SNAPSHOT AND LR UPDATE
        if count % NSNAPSHOT == (NSNAPSHOT - 1):
            lr = generator_optimizer._get_hyper('learning_rate')
            print(f'Updating optimizers learning rate from {lr} to {lr * 0.95}')
            lr = lr * 0.95

            generator_optimizer._set_hyper('learning_rate', lr)
            discriminator_optimizer._set_hyper('learning_rate', lr)

    ## Generate after the final epoch
    # display.clear_output(wait=True)
    generate_and_save_images(generator, epochs, seed, images_folder)


if __name__ == '__main__':
    print("Start training ...")
    train(train_dataset, EPOCHS)
    checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))
    make_gif(anim_file, images_wildcard)
